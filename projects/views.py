from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def project_list(request):
    list_project = Project.objects.filter(owner=request.user)
    context = {
        "list_project": list_project,
    }
    return render(request, "projects/projects_home.html", context)


@login_required
def project_details(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_view(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
