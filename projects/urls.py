from django.urls import path
from projects.views import project_list, project_details, create_view

urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", project_details, name="show_project"),
    path("create/", create_view, name="create_project"),
]
