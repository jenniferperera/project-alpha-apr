from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TasksForm
from tasks.models import Task


@login_required
def create_tasks(request):
    if request.method == "POST":
        form = TasksForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TasksForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_tasks.html", context)


@login_required
def my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": my_tasks,
    }
    return render(request, "tasks/my_tasks.html", context)
